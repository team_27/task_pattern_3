package Cards;

import units.Bouquet;
import units.Unit;

public abstract class Card {
    private String cardType;
    private int deliveryFree = 0;
    private int discount = 0;

    public abstract void useCard(Unit bouquet);
}
