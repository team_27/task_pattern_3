package Cards;

import units.Bouquet;
import units.Unit;

public class DiscountCard extends Card {

    public void useCard(Unit bouquet) {
        bouquet.setDiscount(-100);
    }
}
