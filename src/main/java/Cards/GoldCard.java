package Cards;

import units.Bouquet;
import units.Unit;

public class GoldCard extends Card{
    public void useCard(Unit bouquet) {
        bouquet.setShipPrice(0f);
    }
}
