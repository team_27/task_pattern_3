import Cards.Card;
import Cards.DiscountCard;
import Cards.GoldCard;
import Cards.NullCard;
import factory.FactoryBirthday;
import factory.FactoryFuneral;
import factory.FactoryWedding;
import factory.FlowFactory;
import units.Bouquet;
import units.Unit;

import java.util.ArrayList;
import java.util.Scanner;

public class Customer {
    ArrayList<FlowFactory> factories;
    ArrayList<Unit> bouquets;

    Card card;
    int money;

    public Customer(int money){
        this.money = money;
        factories = new ArrayList();
        factories.add(new FactoryBirthday());
        factories.add(new FactoryWedding());
        factories.add(new FactoryFuneral());

        bouquets = new ArrayList<>();
    }

    public void useCardToPay(Card card){
        getBouquet();
        for(Unit b : bouquets) {
            card.useCard(b);
            if((money -= b.getTotalPrice()) <= 0) {
                System.out.println("You are out of money!");
                System.exit(-1);
            }
            b.ship();
        }
    }

    public void useCardToPay(){
        getBouquet();
        for(Unit b : bouquets) {
            if((money -= b.getTotalPrice()) <= 0) {
                System.out.println("You are out of money!");
                System.exit(-1);
            }
            b.ship();
        }
    }

    public void setCard(String cardType){
        if(cardType.equals("Gold")) {
            card = new GoldCard();
        } else if(cardType.equals("Discount")){
            card = new DiscountCard();
        } else if(cardType.equals("Null")){
            card = new NullCard();
        }
    }

    private void getBouquet() {
        Scanner in = new Scanner(System.in);
        int choice = 0;

        System.out.println("Input type of bouquet:");
        System.out.println("0 - quit");
        System.out.println("1 - for birthday");
        System.out.println("2 - for wedding");
        System.out.println("3 - for funeral");

        choice = in.nextInt();
        try {
            if(choice != 0 ) {
                bouquets.add(factories.get(choice-1).getBouquete());
            } else {
                System.exit(0);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
