package units;

import units.shipment.WaysToShip;
import units.wrapper.Wrappers;

public interface Unit {
    void ship();
    void setWrapper(Wrappers w);
    void setWayToShip(WaysToShip wayToShip);
    void setShipPrice(float price);
    void setDiscount(float discount);
    float getTotalPrice();
}
