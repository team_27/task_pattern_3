package units;

import units.shipment.WaysToShip;
import units.wrapper.Wrappers;

import java.util.ArrayList;

public class Bouquet implements Unit {
    private ArrayList<Flowers> flowers;
    private Wrappers wrapper;
    private WaysToShip wayToShip;

    private float flowersPrice;
    private float shipPrice;
    private float totalPrice;
    private float discount;


    public void setTotalPrice() {
        if(wayToShip != null) {
            setShipPrice(wayToShip.getShipPrice());
        }
        totalPrice = flowersPrice + shipPrice + discount;
    }

    public float getTotalPrice() {
        setTotalPrice();
        return totalPrice;
    }

    public void ship() {
        System.out.println(this);
    }

    public void setWrapper(Wrappers w) {
        wrapper = w;
    }

    public ArrayList<Flowers> getFlowers() {
        return flowers;
    }

    public void setFlowers(ArrayList<Flowers> flowers) {
        this.flowers = flowers;
    }

    public Wrappers getWrapper() {
        return wrapper;
    }

    public WaysToShip getWayToShip() {
        return wayToShip;
    }

    public void setWayToShip(WaysToShip wayToShip) {
        this.wayToShip = wayToShip;
    }

    public float getFlowersPrice() {
        return flowersPrice;
    }

    public void setFlowersPrice(float flowersPrice) {
        this.flowersPrice = flowersPrice;
    }

    public float getShipPrice() {
        return shipPrice;
    }

    public void setShipPrice(float shipPrice) {
        this.shipPrice = shipPrice;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Квіти які входять в букет :");
        result.append(getFlowers());
        result.append("\nОбгортка: ");
        result.append(getWrapper());
        result.append("\nЦіна");
        result.append(getTotalPrice());
        result.append("\nЗнижка:");
        result.append(getDiscount());
        result.append("\nСпосіб доставки: ");
        result.append(getWayToShip());
        result.append("\nЦіна доставки: ");
        result.append(getShipPrice());
        return result.toString();
    }
}
