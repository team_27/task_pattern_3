package units.wrapper;

import units.Unit;

public interface Wrapper {
    void wrap(Unit bouquet);
}
