package units.wrapper;

import units.Unit;

public class RedWrapper implements Wrapper {
    public void wrap(Unit bouquet) {
        bouquet.setWrapper(Wrappers.RED_WRAPPER);
    }
}
