package units;

public enum Flowers {
    ROSE(2.5f),
    TULIP(1.5f),
    CHAMOMILE(1.55f),
    CARNATION(1.7f);

    private float price;
    public float getPrice() {
        return price;
    }
    private Flowers(float p) {
        price = p;
    }
}
