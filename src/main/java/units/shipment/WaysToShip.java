package units.shipment;

public enum WaysToShip {
    BY_LIMO(250f),
    BY_CAR(150f);

    private float shipPrice;
    public float getShipPrice() {
        return shipPrice;
    }
    private WaysToShip(float price) {
        shipPrice = price;
    }
}
