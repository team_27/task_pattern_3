package units.shipment;

import units.Unit;

public interface Provider {
    void provide(Unit bouquet);
}
