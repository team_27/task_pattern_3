package units.shipment;

import units.Bouquet;
import units.Unit;

public class CarProvider implements Provider {

    public void provide(Unit bouquet) {
        bouquet.setWayToShip(WaysToShip.BY_CAR);
    }
}
