package units.decorator;

import units.Unit;
import units.shipment.WaysToShip;
import units.wrapper.Wrappers;

public class Discount extends Decorator {

    private float discount;

    public Discount(Unit bouquet, float discount) {
        super(bouquet);
        this.discount = discount;
    }

    @Override
    public void ship() {
        setDiscount(discount);
        super.ship();
    }

    @Override
    public void setWrapper(Wrappers wrappers) {
        bouquet.setWrapper(wrappers);
    }

    @Override
    public void setWayToShip(WaysToShip wayToShip) {
        bouquet.setWayToShip(wayToShip);
    }

    @Override
    public void setShipPrice(float price) {
        bouquet.setShipPrice(price);
    }

    @Override
    public void setDiscount(float discount) {
        bouquet.setDiscount(discount);
    }

    @Override
    public float getTotalPrice() {
        return bouquet.getTotalPrice();
    }
}
