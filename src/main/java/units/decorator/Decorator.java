package units.decorator;

import units.Unit;
import units.shipment.WaysToShip;
import units.wrapper.Wrappers;

public abstract class Decorator implements Unit {

    public Decorator(Unit bouquet) {
        this.bouquet = bouquet;
    }

    protected Unit bouquet;
    @Override
    public void setWrapper(Wrappers wraps) {
        bouquet.setWrapper(wraps);
    }

    @Override
    public void setWayToShip(WaysToShip wayToShip) {
        bouquet.setWayToShip(wayToShip);
    }

    @Override
    public void setDiscount(float discount) {
        bouquet.setDiscount(discount);
    }

    public void ship() {
        bouquet.ship();
    }
}
