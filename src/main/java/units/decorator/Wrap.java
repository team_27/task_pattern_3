package units.decorator;

import units.Unit;
import units.shipment.WaysToShip;
import units.wrapper.Wrapper;
import units.wrapper.Wrappers;

public class Wrap extends Decorator {

    public Wrap(Unit bouquet, Wrapper wrapper) {
        super(bouquet);
        this.wrapper = wrapper;
    }
    private Wrapper wrapper;

    @Override
    public void ship() {
        wrapper.wrap(bouquet);
        bouquet.ship();
    }

    @Override
    public void setWrapper(Wrappers w) {
        bouquet.setWrapper(w);
    }

    @Override
    public void setWayToShip(WaysToShip wayToShip) {
        bouquet.setWayToShip(wayToShip);
    }

    @Override
    public void setDiscount(float discount) {
        bouquet.setDiscount(discount);
    }

    @Override
    public void setShipPrice(float price) {
        bouquet.setShipPrice(price);
    }

    @Override
    public float getTotalPrice() {
        return bouquet.getTotalPrice();
    }
}
