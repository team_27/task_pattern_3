package units.decorator;

import units.Unit;
import units.shipment.Provider;
import units.shipment.WaysToShip;
import units.wrapper.Wrappers;

public class Deliver extends Decorator {
    private Provider provider;

    public Deliver(Unit bouquet, Provider provider) {
        super(bouquet);
        this.provider = provider;
    }

    @Override
    public void ship() {
        provider.provide(bouquet);
        bouquet.ship();
    }

    @Override
    public void setWrapper(Wrappers wrappers) {
        bouquet.setWrapper(wrappers);
    }

    @Override
    public void setWayToShip(WaysToShip wayToShip) {
        bouquet.setWayToShip(wayToShip);
    }

    @Override
    public void setShipPrice(float price) {
        bouquet.setShipPrice(price);
    }

    @Override
    public void setDiscount(float discount) {
        super.setDiscount(discount);
    }

    @Override
    public float getTotalPrice() {
        return bouquet.getTotalPrice();
    }

}
