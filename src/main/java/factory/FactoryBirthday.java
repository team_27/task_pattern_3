package factory;

import units.Bouquet;
import units.Flowers;
import units.Unit;
import units.decorator.Wrap;
import units.wrapper.GreenWrapper;
import units.wrapper.Wrappers;

import java.util.*;

public class FactoryBirthday implements FlowFactory {
    private ArrayList<Flowers> flowersList = new ArrayList<>();
    private Bouquet bouquet;
    private String factoryBirthdayName = "Квіти на день народження!";
    public FactoryBirthday() {
        flowersList.add(Flowers.ROSE);
        flowersList.add(Flowers.TULIP);
    }
    @Override
    public Unit getBouquete() {
        bouquet = new Bouquet();
        bouquet.setFlowers(flowersList);
        bouquet.setFlowersPrice(245);
        Wrap wrap = new Wrap(bouquet, new GreenWrapper());
        System.out.println(factoryBirthdayName);
        return wrap;
    }


}
