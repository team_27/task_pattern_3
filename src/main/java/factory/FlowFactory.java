package factory;

import units.Unit;

public interface FlowFactory {
    public Unit getBouquete();
}
