package factory;

import units.Bouquet;
import units.Flowers;
import units.Unit;
import units.decorator.*;
import units.shipment.LimoProvider;
import units.wrapper.*;

import java.util.ArrayList;

public class FactoryWedding implements FlowFactory {
    private ArrayList<Flowers> flowersList = new ArrayList<>();
    private Bouquet bouquet;
    private String factoryWeddingName = "Квіти на Весілля!";
    public FactoryWedding() {
        flowersList.add(Flowers.CARNATION);
        flowersList.add(Flowers.CARNATION);
    }
    @Override
    public Unit getBouquete() {
        bouquet = new Bouquet();
        bouquet.setFlowers(flowersList);
        bouquet.setFlowersPrice(245);
        System.out.println(factoryWeddingName);
        Wrap wrap = new Wrap(new Deliver(bouquet, new LimoProvider()), new YellowWrapper());
        return wrap;
    }
}
