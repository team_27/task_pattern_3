package factory;

import units.Bouquet;
import units.Flowers;
import units.Unit;
import units.decorator.Deliver;
import units.decorator.Wrap;
import units.shipment.CarProvider;
import units.wrapper.RedWrapper;
import units.wrapper.Wrappers;

import java.util.ArrayList;

public class FactoryFuneral implements FlowFactory {
    private ArrayList<Flowers> flowersList = new ArrayList<>();
    private Bouquet bouquet;
    private String factoryFuneralName = "Квіти на похорони!";
    public FactoryFuneral() {
        flowersList.add(Flowers.ROSE);
        flowersList.add(Flowers.CHAMOMILE);
    }
    @Override
    public Unit getBouquete() {
        bouquet = new Bouquet();
        bouquet.setFlowers(flowersList);
        bouquet.setFlowersPrice(225);
        System.out.println(factoryFuneralName);
        Wrap wrap = new Wrap(new Deliver(bouquet, new CarProvider()), new RedWrapper());
        return wrap;
    }

}
